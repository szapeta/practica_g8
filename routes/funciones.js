const { Router } = require('express');
const { funcionesGet,
    funcionesPut,
    funcionesPost,
    funcionesDelete } = require('../controllers/funciones');

const router = Router();

router.get('/', funcionesGet);

router.put('/:id', funcionesPut);

router.post('/', funcionesPost);

router.delete('/:id', funcionesDelete);

module.exports = router;