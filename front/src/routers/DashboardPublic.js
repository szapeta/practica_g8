import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Alrevez } from '../components/public/alrevez/Alrevez';
import { Fibonnacci } from '../components/public/fibonnacci/Fibonnacci';

import { Home } from "../components/public/home/Home";
import { Parimpar } from '../components/public/parimpar/Parimpar';
import { Potencia } from '../components/public/potencia/Potencia';
import { Raiz } from '../components/public/raiz/Raiz';

import Footer from '../components/ui/Footer';
import Navbar from '../components/ui/Navbar';

export const DashboardPublic = () => {
  return (
    <>
    <Navbar/>
      <div className='container mt-2'>
      <Switch>
        <Route path="/home" component={Home} />
        <Route path="/parimpar" component={Parimpar} />
        <Route path="/fibonnacci" component={Fibonnacci} />
        <Route path="/alrevez" component={Alrevez} />
        <Route path="/potencia" component={Potencia} />
        <Route path="/raiz" component={Raiz} />
        <Route path="/" component={Home} />
      </Switch>
      </div>
      <Footer/>
    </>
  );
};
