import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import { Link, useHistory } from "react-router-dom";
import { AuthContext } from "../../auth/AuthContext";
import { types } from "../../types/Types";
import { NavClear } from "./NavClear";
import { NavUsuario } from "./NavUsuario";

export const NavAuth = () => {
    const {
        user: { name, rol },
        dispatch,
    } = useContext(AuthContext);

    const history = useHistory();

    const handleLogout = () => {
        history.replace("/home");
        dispatch({
            type: types.logout,
        });
    };

    return (
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
            <Link className="navbar-brand" to="/homeuser">
                Practica1
            </Link>

            {rol == types.rolUsuario ? <NavUsuario /> : <NavClear />}

            <div className="navbar-nav">
                <NavLink
                    activeClassName="active"
                    className="nav-item nav-link"
                    exact
                    to="/login"
                >
                    {name}
                </NavLink>
            </div>
            <div className="navbar-nav">
                <NavLink
                    activeClassName="active"
                    className="nav-item nav-link"
                    exact
                    to=""
                >
                    |
                </NavLink>
            </div>
            <div className="navbar-nav">
                <button
                    className="nav-item nav-link btn"
                    onClick={handleLogout}
                >
                    Salir
                </button>
            </div>
        </nav>
    );
};
