import axios from 'axios';
import React, { useState } from 'react'
import { types } from '../../../types/Types';

export const Parimpar = () => {

  
  //estados para guardar las variables
  const [palabra, setPalabra] = useState("");
  const [resultado, setResultado] = useState("")

  //funcion para llamar a los endpoint
  const handlecalcular = async(e)=>{
      e.preventDefault()

      //types.apiurl = http://localhost:300 <-- configurado en el archivo /types/Types.js
      axios.post(types.apiurl+"paroimpar",{
          palabra
      }).then(result=>{

          console.log(result)
          //lectura del valor que devuelve el endpoin
          let valData = result.data;
          let valRaiz = valData.raiz;
          
          //se guarda el valor en el estado se usa: set
          setResultado(valData);
      })
  }

  return (
    <div>Parimpar CAMVBIO</div>
  )
}
